FROM archlinux:latest

RUN pacman -Syu --noconfirm && \
  pacman -S --noconfirm reflector && \
  reflector --latest 50 --sort rate --protocol https --save /etc/pacman.d/mirrorlist && \
  pacman -S --noconfirm base-devel git

RUN useradd -mU builder && \
  echo 'builder ALL=(ALL) NOPASSWD: ALL' >/etc/sudoers.d/builder && \
  chmod 440 /etc/sudoers.d/builder && \
  visudo -c

RUN mkdir /aur && \
  chown builder:builder /aur

COPY build-packages /usr/bin/build-packages

RUN INSTALL_ONLY=1 build-packages python311
ENV CLOUDSDK_PYTHON=python3.11
RUN INSTALL_ONLY=1 build-packages google-cloud-cli
