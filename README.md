# arch-packages

Build Arch Linux AUR packages in CI, hosted in a GCS bucket.

## Bootstrapping

1. Create a GCS bucket.
1. Create a GCP service account and private key.
1. IAM-bind the service account to storage object admin for the bucket.
1. Add allUsers as legacy object viewers of the bucket.
   1. This makes objects public, while denying listing.
1. Generate a UUID for the bucket path.
   1. Keep the bucket name and/or path secret. We need to keep the bucket
      publicly readable because pacman cannot authenticate with it, but we don't
      want anyone downloading lots of objects, causing us to pay egress charges.
1. Run the following commands:

   ```
   touch "${REPO_NAME}.db.tar.zst"
   touch "${REPO_NAME}.files.tar.zst"
   gsutil cp "${REPO_NAME}.db.tar.zst" "gs://${BUCKET_NAME}/${BUCKET_PATH}/${REPO_NAME}.db"
   gsutil cp "${REPO_NAME}.files.tar.zst" "gs://${BUCKET_NAME}/${BUCKET_PATH}/${REPO_NAME}.files"
   ```
1. Configure the GitLab CI variables documented in
   [`build-packages`](build-packages).
   1. To avoid secret leakage, make pipelines members-only.
   1. As an extra layer, mask the bucket name and path.
